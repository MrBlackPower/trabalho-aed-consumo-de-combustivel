#include <iostream>
#include <stdio.h>
#include <string>

struct Post
{
	int distance;
	double price;
};

std::vector<Post> postos;
int n = 0;
int combustivel_total = 0;

bool jumpReadLine(char *line, FILE *file){
    if(fgets(line, 80, file) != NULL)
        return true;

    return false;
}

bool loadRaw(string filename){ 
	FILE *file;

    char* fileName = filename.toUtf8().data();
    file = fopen(fileName,"r");

    if(file == NULL){
        clear();
        return false;
    }

    if(fgets(line, 80, file) != NULL)
    	return NULL;

    stringstream s(line);
    /**
      * COM?BUSTÍVEL INICIAl
      *********************/
    s >> combustivel_total ;

	jumpReadLine();

    s >> n;

    while(fgets(line, 80, file) != NULL){
    	stringstream s(line);
        /**
          * Postos
          *********************/
    	Post p;
    	s >> p.distance >> p.price;

    	Postos.pusch_back(p);
    }

    if(Postos.size() != n)
    	cout << "LEITURA ERRÔNEA !" << endl;

    fclose(file);

    return true;
}


bool saveRaw(vector<string> raw, char *fileName){
    FILE *file;
    file = fopen(fileName,"w+");

    if(file == NULL)
        return false;

    for(int i = 0; i < raw.size(); i++){
        QString line = raw[i];
        fprintf(file,"%s\n", line.data());
    }

    fclose(file);

    return true;
}

void main(int argc, char[] args){
	vector<string> r;

	loadRaw("text.txt");


	/*** 
	* UTILIZA DADOS CARREGADOS
	 ***/

	r.pusch_back("RESULTADO");
}